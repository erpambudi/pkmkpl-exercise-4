# PKMKPL Exercise 4

Sumber latihan ini di dapatkan dari buku :

[Test-Driven Development with Python, by Harry Percival O Reilly, 2017](https://www.obeythetestinggoat.com/pages/book.html)

Project Sebelumnya dapat di akses di
[Exercise 3](https://gitlab.com/erpambudi/pkmkpl-exercise-3.git)

Project Django ini adalah sebuah halaman website sederhana dan masih berada di local server.
Untuk melihat halaman website yang sudah di deploy bisa di akses di :
https://erpamweb.herokuapp.com/


Jika website tidak bisa di buka, kemungkinan website dalam mode sleep. Cobalah untuk me refresh website secara berulang.



Di dalam Latihan Exercise 4 kali ini yaitu :

1.  Penambahan CSS pada website Django
2.  Penambahan framework Bootstrap 4 pada website Django
3.  Pembuatan functional test untuk pengujian CSS dan Layouting element pada website
4.  Hubungan latihan ini dengan Bab 7 pada buku acuan

Pada latihan kali ini dibuat suatu CSS yang dapat di jalankan oleh test functional_tests dan yang tidak bisa di terima oleh test tersebut.



**CSS yang di terima functional_tests**

Di bawah ini merupakan CSS yang dapat di terima oleh functional_tests, CSS tersebut berfungsi untuk meletakan element tabel di tengah - tengah halaman website.


```
.id_item_center {
            text-align: center;
            align-items: center;
        }

        .table1 {
            font-family: sans-serif;
            color: #444;
            border-collapse: collapse;
            width: 100%;
            border: 1px solid #f2f5f7;
        }
 
        .table1 tr th{
            background: #35A9DB;
            color: #fff;
            font-weight: normal;
        }
        
        .table1, th, td {
            padding: 8px 20px;
            text-align: center;
        }
        
        .table1 tr:hover {
            background-color: #f5f5f5;
        }
        
        .table1 tr:nth-child(even) {
            background-color: #f2f2f2;
        }
        
        
```

Kemudian dibuat fungsi untuk melakukan pengecekan CSS yang bernama `test_layout_and_styling_table()`
Fungsi ini untuk melakukan pengecekan posisi element berada di tengah halaman website atau tidak. element yang di cek adalah element tabel dengan id : `id_todo_table`
Jika test di jalankan, maka test akan berhasil dan di terima, karena CSS di atas merupakan CSS untuk meletakan tabel di tengah halaman.

```
def test_layout_and_styling_table(self):
        
        self.browser.get(self.live_server_url)
        
        #inisialisasi id tabel to do list
        table = self.browser.find_element_by_id('id_todo_table')

        # melakukan pengecekan posisi tabel di tengah layout atau tidak
        self.assertAlmostEqual(
            table.location['x'] + table.size['width']/2,
            512,
            delta=10
        )

        #jika semuanya true maka test berhasil dan akan menampilkan pesan di CMD
        self.fail('OK Finish the test!')
```

**CSS yang tidak di terima functional_test**

Kemudian di bawah ini merupakan CSS untuk mengatur elemen yang menampilkan komentar pada website. CSS ini tidak di terima oleh functional_tests. CSS di bawah ini merupakan CSS yang mengatur element agar berada paling kiri halaman.
hal ini sangat tidak enak di lihat karena tidak rapih dan tidak selaras dengan elemen - elemen lainnya.

```
.comment{
            padding: 8px 20px;
            text-align: left;
            justify-content: left;
            background-color: #f2f2f2;
        }
        
```

Karena CSS tersebut tidak memenuhi syarat pada functional test yang telah di buat maka functional_tests akan menampilkan pesan error seperti di bawah ini :

```
self.assertAlmostEqual(
AssertionError: 44.0 != 512 within 10 delta (468.0 difference)
```

**Hubungan antara exercise kali ini dengan Bab 7 buku acuan**

Pada bab 7 buku acuan mempraktekan bagaimana membuat halaman website baru, dimana nantinya untuk melakukan testing disana. selain itu di dalamnya di praktekan bagaimana melakukan GET dan POST pada website yang di buat.
Hubungan antara bab 7 dengan exercise kali ini adalah mengubah kode halaman baru yang telah di buat ke dalam bentuk html baru yang menggunakan Bootstrap.
